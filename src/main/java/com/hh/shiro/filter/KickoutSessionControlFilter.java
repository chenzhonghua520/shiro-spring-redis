package com.hh.shiro.filter;

import java.io.Serializable;
import java.util.Deque;
import java.util.LinkedList;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.SessionException;
import org.apache.shiro.session.mgt.DefaultSessionKey;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.AccessControlFilter;
import org.apache.shiro.web.util.WebUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hh.shiro.entity.ShiroUser;

public class KickoutSessionControlFilter extends AccessControlFilter {
	private static Logger LOGGER = LoggerFactory.getLogger(KickoutSessionControlFilter.class);
	
	private String kickoutUrl;              // 踢出后的url
	private boolean kickoutAfter = false;   // 是否把后来登录的人踢出
	private int maxSession = 1;             // 同一帐号最大会话数
    private SessionManager sessionManager;
    private CacheManager cacheManager;
	private final String kickOutKeyPrefix = "shiro-kickout-session";
    
	private static Cache<String, Deque<Serializable>> kickOutCache = null;
	
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
    	if(null == kickOutCache){
    		kickOutCache = cacheManager.getCache(kickOutKeyPrefix);
    	}
        Subject subject = getSubject(request, response);
        Session session = subject.getSession();
        if (!subject.isAuthenticated() && !subject.isRemembered()) { // 如果没有登录，直接进行之后的流程
			return true;
		}
		if (session.getAttribute("kickout") != null) {
			return false;
		}
        ShiroUser user = (ShiroUser) subject.getPrincipal();
        String mobilePhone = null;
		if (null != user && StringUtils.isNotBlank(user.getUsername())) {
			mobilePhone = user.getUsername();
		}
        Serializable sessionId = session.getId();
        Deque<Serializable> deque = kickOutCache.get(mobilePhone);
        if(deque == null) {
            deque = new LinkedList<Serializable>();
        }
        if (!deque.contains(sessionId) && session.getAttribute("kickout") == null) {  // 如果队列里没有此sessionId，且用户没有被踢出；放入队列
			deque.push(sessionId);
		}
        kickOutCache.put(mobilePhone, deque);
        if(deque.size() > maxSession){
        	return false;
        }
        return true;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
    	Subject subject = getSubject(request, response);
        Session session = subject.getSession();
        ShiroUser user = (ShiroUser) subject.getPrincipal();
        String username = null;
		if (null != user && StringUtils.isNotBlank(user.getUsername())) {
			username = user.getUsername();
		}
        Deque<Serializable> deque = kickOutCache.get(username); // 相同帐号登录要同步控制，不同帐号无需考虑
		while (deque.size() > maxSession) {             // 如果队列里的sessionId数超出最大会话数，开始踢人
			Serializable kickoutSessionId = null;
			Session kickoutSession = null;
			if (kickoutAfter) {                         // 如果踢出后者
				kickoutSessionId = deque.removeFirst();
			} else {                                    // 否则踢出前者
				kickoutSessionId = deque.removeLast();
			}
			if (null != kickoutSessionId) {
				try {
					kickoutSession = sessionManager.getSession(new DefaultSessionKey(kickoutSessionId));
				} catch (SessionException e) {
					LOGGER.debug("存储在redis中的session:[{}]已经消亡了", kickoutSessionId);
					continue;
				}
			}
			if (kickoutSession != null) {
				kickoutSession.setAttribute("kickout", true); // 设置会话的kickout属性表示踢出了
			}
		}
		kickOutCache.put(username, deque);
        if (session.getAttribute("kickout") != null) {        //如果被踢出了，直接退出，重定向到踢出后的地址
			try {
				subject.logout();
				LOGGER.info("踢出用户,用户信息为：[{}]", user);
			} catch (Exception e) {
				LOGGER.info(e.getMessage(), e);
			}
            saveRequest(request);
            WebUtils.issueRedirect(request, response, kickoutUrl);
            return false;
        }
        return true;
    }


	public void setKickoutUrl(String kickoutUrl) {
		this.kickoutUrl = kickoutUrl;
	}

	public void setKickoutAfter(boolean kickoutAfter) {
		this.kickoutAfter = kickoutAfter;
	}

	public void setMaxSession(int maxSession) {
		this.maxSession = maxSession;
	}

	public void setSessionManager(SessionManager sessionManager) {
		this.sessionManager = sessionManager;
	}

	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
    
}